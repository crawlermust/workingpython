import boto3
import time

bucket_name='bucketinho'
def create_bucket(bucket_name):
    s3 = boto3.client('s3')
    response = s3.create_bucket(
        Bucket=bucket_name
    )

def enable_website_hosting(bucket_name):
    s3 = boto3.client('s3')
    s3.put_bucket_website(
        Bucket=bucket_name,
        WebsiteConfiguration={
            'ErrorDocument':{
                'Key': 'error.html'
            },
            'IndexDocument': {
                'Suffix': 'index.html'
            }
        }

    )

def put_bucket_encryption(bucket_name):
    s3 = boto3.client('s3')
    s3.put_bucket_encryption(
        Bucket=bucket_name,
        ServerSideEncryptionConfiguration={
            'Rules': [
                {
                    'ApplyServerSideEncryptionByDefault': {
                        'SSEAlgorithm': 'AES256'
                    }
                }
            ]
        }
    )

def put_bucket_versioning(bucket_name):
    s3 = boto3.client('s3')
    s3.put_bucket_versioning(
        Bucket=bucket_name,
        VersioningConfiguration={
            'MFADelete': 'Disabled',
            'Status': 'Enabled',
        },
    )

create_bucket(bucket_name)
enable_website_hosting(bucket_name)
put_bucket_encryption(bucket_name)
put_bucket_versioning(bucket_name)

def create_key_pair():
    ec2_kp = boto3.client('ec2', region_name = 'us-east-1')
    ec2_kp.create_key_pair(
        KeyName = 'senaikey',
        KeyType = 'rsa',
        KeyFormat = 'pem'
    )

create_key_pair()
time.sleep(25)

UserData_Script = '''
#!/bin/bash
yum update -y
yum install nginx -y
systemctl start nginx
'''

def create_instance():
    ec2 = boto3.client('ec2', region_name = 'us-east-1')

    instance = ec2.run_instances(
        ImageId = 'ami-06b09bfacae1453cb',
        InstanceType = 't3.micro',
        KeyName = 'senaikey',
        MaxCount = 1,
        MinCount = 1,
        UserData = UserData_Script
    )
create_instance()

##teste
